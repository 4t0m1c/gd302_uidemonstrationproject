using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputListenAnyKey : MonoBehaviour {

    [SerializeField] UnityEvent OnAnyKeyPressed = new UnityEvent ();
    UIInput uiInput;

    void OnEnable () {
        uiInput = new UIInput ();
        uiInput.Enable ();

        uiInput.UI.AnyKey.performed += x => OnAnyKeyPressed.Invoke ();
    }

    void OnDisable () {
        uiInput.Dispose ();
    }

}