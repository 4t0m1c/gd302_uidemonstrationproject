using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class UIFadeInOut : MonoBehaviour {

    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] UnityEvent OnComplete = new UnityEvent ();
    [SerializeField] float waitFirst = 2;
    [SerializeField] bool fadeIn = false;
    [SerializeField] bool fadeOut = false;

    bool playedOnce = false;

    IEnumerator Start () {
        if (!playedOnce) {
            playedOnce = true;

            yield return new WaitForSeconds (waitFirst);

            if (fadeIn) {
                canvasGroup.DOFade (1, 1).OnComplete (() => {
                    if (fadeOut) {
                        canvasGroup.DOFade (0, 1).SetDelay (2).OnComplete (() => {
                            OnComplete.Invoke ();
                        });
                    } else {
                        OnComplete.Invoke ();
                    }
                });
            } else if (fadeOut) {
                canvasGroup.DOFade (0, 1).OnComplete (() => {
                    OnComplete.Invoke ();
                });
            }
        }
    }

}