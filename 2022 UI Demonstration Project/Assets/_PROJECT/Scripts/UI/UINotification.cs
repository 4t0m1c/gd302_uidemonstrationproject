using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UINotification : MonoBehaviour {

    [SerializeField] RectTransform contentRect;


    [ContextMenu("Slide In")]
    void SlideIn () {
        contentRect.DOPivotX (1, 0.5f);
    }

    [ContextMenu("Slide Out")]
    void SlideOut () {
        contentRect.DOPivotX (0, 0.5f);
    }

}